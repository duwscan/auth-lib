# authen-lib

## A shared-libary for Spring Boot microservice learning project

### Requirements:

- Maven
- Java SDK 18
- Inteliji IDE

### Installation:

1. Clone this repo
   ```
   git clone https://gitlab.com/duwscan/auth-lib.git
   ```
2. Cd/Open this project and run Maven commands using the Maven Plugin in your IDE
   ```
   mvn clean package
   mvn clean install
   ```
3. Open your project's `pom.xml` and add the following lines:
   ```xml
   <dependency>
            <groupId>com.web.authlib</groupId>
            <artifactId>auth-lib</artifactId>
            <version>1.0-SNAPSHOT</version>
   </dependency>
   ```
4. Reload All Maven project and Done!

### Authentication Flow Diagram:

![Flow Diagram](/authen-resource-flow.drawio.png)

### Implementation

1. Add introspect endpoint and jwt secret of auth-server in your application.yml/application.properties:
   ```yml
      application:
        security:
          jwt:
             secret-key: [auth-server's jwt secrect key]
          auth-server:
             url: [auth-server's introspect endpoint]
   ```
2. Create a Config File that tells your app to scan bean in this lib
   ```java
   @Configuration
   @EnableAutoConfiguration
   @ComponentScan(basePackages = "com.web.authlib")
   @RequiredArgsConstructor
   public class AuthConfig {
        // jwtService to extract Claims in Token 
        private final JwtService jwtService;
        // You can custom your mapper or use SecurityUserBuilder built-in class
        @Bean
        SecurityUserMapper securityUserMapper() {
            return new SecurityUserBuilder(jwtService);
        }
   }
   ```
3. Inject AccessTokenAuthenticationFilter in SecurityConfig:
   ```java
   @Service
   @Configuration
   @RequiredArgsConstructor
   public class SecurityConfig {
    private final AccessTokenAuthenticationFilter accessTokenAuthenticationFilter;
       @Bean
       public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
           return http.addFilterAt(accessTokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class).authorizeHttpRequests(authorize -> {
               authorize.anyRequest().hasRole("USER");//define more authorize logic
           }).build();
       }
   }
   
   ```
   

