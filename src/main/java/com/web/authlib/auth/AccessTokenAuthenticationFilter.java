package com.web.authlib.auth;

import com.web.authlib.auth.jwt.JwtAuthorizationHeader;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import com.web.authlib.exception.UnauthorizedException;

import java.io.IOException;

@Component
@AllArgsConstructor
public class AccessTokenAuthenticationFilter extends OncePerRequestFilter {
    private final AccessTokenAuthenticationManager authenticationManager;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        JwtAuthorizationHeader authHeader = new JwtAuthorizationHeader(request);
        try {
            String token = authHeader.getBearerToken();
            AccessTokenAuthentication accessTokenAuthentication = new AccessTokenAuthentication(false, token);
            var a = (AccessTokenAuthentication) authenticationManager.authenticate(accessTokenAuthentication);
            SecurityContextHolder.getContext().setAuthentication(a);
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            throw new UnauthorizedException();
        }
    }
}
