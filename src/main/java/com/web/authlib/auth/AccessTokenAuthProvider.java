package com.web.authlib.auth;

import lombok.*;
import com.web.authlib.auth.jwt.JwtAuthorizationHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import com.web.authlib.auth.jwt.SecurityUserMapper;
import com.web.authlib.exception.UnauthorizedException;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class AccessTokenAuthProvider implements AuthenticationProvider {
    @Value("${application.security.auth-server.url}")
    private String AUTHENTICATION_SERVER;
    private final SecurityUserMapper securityUserMapper;

    public AccessTokenAuthProvider(final SecurityUserMapper securityUserMapper) {
        this.securityUserMapper = securityUserMapper;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        AccessTokenAuthentication at = (AccessTokenAuthentication) authentication;
        String jwtToken = at.getAccessToken();
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(AUTHENTICATION_SERVER))
                .GET()
                .header(HttpHeaders.AUTHORIZATION, JwtAuthorizationHeader.BEARER_TOKEN_PREFIX + jwtToken)
                .build();
        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (!(response.statusCode() >= 200 && response.statusCode() <= 299)) {
                throw new Exception("Unauthorized");
            }
            if (securityUserMapper == null) throw new Exception("Required SecurityUserMapper.class");
            AccessTokenAuthentication a = new AccessTokenAuthentication(true, jwtToken, securityUserMapper.mapper(jwtToken));
            return a;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new UnauthorizedException();
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return AccessTokenAuthentication.class.equals(authentication);
    }
}
