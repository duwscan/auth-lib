package com.web.authlib.auth;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AccessTokenAuthenticationManager implements AuthenticationManager {
    private final AccessTokenAuthProvider provider;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (provider.supports(authentication.getClass())) {
            Authentication a = provider.authenticate(authentication);
            if (a.isAuthenticated()) {
                return a;
            }
        }
        throw new BadCredentialsException("Oh No! Invalid Token");
    }
}
