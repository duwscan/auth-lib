package com.web.authlib.auth.jwt;

import com.web.authlib.auth.SecurityUser;

@FunctionalInterface
public interface SecurityUserMapper {
    SecurityUser mapper(String token);
}
