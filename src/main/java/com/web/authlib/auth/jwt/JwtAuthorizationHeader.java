package com.web.authlib.auth.jwt;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;

public class JwtAuthorizationHeader {
    public final static String BEARER_TOKEN_PREFIX = "Bearer ";
    private final String header;

    public JwtAuthorizationHeader(HttpServletRequest request) {
        header = request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    public JwtAuthorizationHeader(String header) {
        this.header = header;
    }

    public boolean isAuthorizationHeader() {
        return header != null;
    }

    public boolean hasBearerToken() {
        return header.startsWith(BEARER_TOKEN_PREFIX);
    }

    public String getBearerToken() throws Exception {
        if (!hasBearerToken() || !isAuthorizationHeader()) {
            throw new Exception("Invalid Authorization Request Header");
        }
        return header.substring(BEARER_TOKEN_PREFIX.length());
    }
}
