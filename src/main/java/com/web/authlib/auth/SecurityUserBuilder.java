package com.web.authlib.auth;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import com.web.authlib.auth.jwt.JwtService;
import com.web.authlib.auth.jwt.SecurityUserMapper;

import java.util.List;

@Component
@AllArgsConstructor
public class SecurityUserBuilder implements SecurityUserMapper {
    private JwtService jwtService;

    @Override
    public SecurityUser mapper(String token) {
        String username = jwtService.extractUsername(token);
        List<String> roles = jwtService.extractRoles(token);
        return SecurityUser.builder()
                .roles(roles)
                .username(username)
                .build();
    }
}
