package com.web.authlib.auth;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
public class SecurityUser {
    private List<String> roles;
    private String username;
}
