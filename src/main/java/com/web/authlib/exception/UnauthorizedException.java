package com.web.authlib.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends CustomException {
    public UnauthorizedException(String msg) {
        super();
        setDefaultCustomMessage(msg);
        this.setStatusCode(HttpStatus.UNAUTHORIZED);
    }

    public UnauthorizedException() {
        super();
        setDefaultCustomMessage("Unauthorized");
        this.setStatusCode(HttpStatus.UNAUTHORIZED);
    }
}

