package com.web.authlib.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@NoArgsConstructor
public class CustomException extends RuntimeException {
    private HttpStatus statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    private String defaultCustomMessage;

    public CustomException(String message, HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public CustomException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        String messageDetail = super.getMessage();
        return messageDetail != null ? messageDetail : this.defaultCustomMessage;
    }
}
