package com.web.authlib.response.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ResponseHandler {
    public static ResponseEntity<Object> response(HttpStatus status, Object msg, Object error, Object objectResponse) {
        Map<String, Object> response = new HashMap<>();
        response.put("_data", objectResponse);
        response.put("_message", msg);
        if (error != null) {
            response.put("_error", error);
        }
        response.put("_httpStatus", status.value());
        return new ResponseEntity<>(response, status);
    }

    public static Map<String, Object> exceptionResponseBody(Object status, Object error) {
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("timestamp", new Date());
        responseBody.put("_httpStatus", status);
        responseBody.put("_error", error);
        return responseBody;
    }
}
